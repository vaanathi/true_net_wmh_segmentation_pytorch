#!/usr/bin/env python
#   Copyright (C) 2016 University of Oxford 
#   SHBASECOPYRIGHT

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import torch
import torch.nn as nn
from torch import optim
#from tqdm import tqdm
import os
from utils import *
from scipy.ndimage import filters
import math
#from torch.utils.tensorboard import SummaryWriter
from skimage import exposure
import augmentations_gmdist
import augmentations_distmaps2, augmentations_distmaps, augmentations_distmaps_t1
from skimage.transform import resize
import data_preprocessing
import model_layers_functions_torch 
from loss_functions_torch import *
import data_preparation
import data_preparation_pytorch
import glob
import data_postprocessing
from Weighted_TrUE_Net_withT1 import UNetAxial, UNetSagittal, UNetCoronal
from Weighted_TrUE_Net_withT1_condensed import TrUENet

def append_data(testdata):
    for d in range(len(testdata)):
        da = testdata[d]
        if len(da.shape) == 4:
            extra = np.zeros([8,da.shape[1],da.shape[2],da.shape[3]])
        else:
            extra = np.zeros([8,da.shape[1],da.shape[2]])
        da = np.concatenate([da,extra],axis=0)
        testdata[d] = da
    return testdata

def dice_coeff(inp, tar):
    smooth = 1.
    pred_vect = inp.contiguous().view(-1)
    target_vect = tar.contiguous().view(-1)
    intersection = (pred_vect * target_vect).sum()
    dice = (2. * intersection + smooth) / (torch.sum(pred_vect) + torch.sum(target_vect) + smooth)
    return dice

def train_truenet(train_folder, val_folder, model, batch_size, num_epochs, device, mode='axial', save_checkpoint = True):
    dir_checkpoint = '/gpfs2/well/win/users/tjj573/checkpoints/'
    train_names = glob.glob(train_folder+'*.npz')
    val_names = glob.glob(val_folder+'*.npz')
    batch_factor = 10 # determines how many images are loaded for training at an iteration
    num_iters = max(len(train_names)//batch_factor,1)
    losses = []
    losses_val = []
    lrt = 0.001
    optimizer = optim.Adam(model.parameters(), lr=lrt, eps=1e-04)
    scheduler = optim.lr_scheduler.MultiStepLR(optimizer, [1], gamma=0.2, last_epoch=-1)
    criterion = CombinedLoss()
    #writer = SummaryWriter(comment=f'LR_{lrt}_BS_{batch_size}')
    gstep = 0
    print('Training started!!.......................................')
    for epoch in range(num_epochs):
        model.train()
        running_loss = 0.0       
        print('Epoch: ' + str(epoch+1) + 'starting!..............................')
        for i in range(num_iters):
            trainnames = train_names[i*batch_factor:(i+1)*batch_factor]
            print('Training files names listing...................................')
            print(trainnames)
            brains, data, data_t1, labels, GM_distance, ventdistmap = data_preparation.create_data_array_from_loaded_data(trainnames, plane=mode)
            traindata = data_preparation.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap)
            brains, data, data_t1, labels, GM_distance, ventdistmap = data_preparation.create_data_array_from_loaded_data(val_names, plane=mode)
            valdata = data_preparation.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap)
            numsteps = traindata[0].shape[0]//batch_size
            gen_next_train_batch = batch_generator(traindata, batch_size, shuffle=True)
            #gen_next_val_batch = batch_generator(valdata, batch_size, shuffle=False)
            for j in range(300):                
                model.train()
                X, y, pwg, pwv = next(gen_next_train_batch)
                X = X.transpose(0,3,1,2)
                print('Training dimensions.......................................')
                print(X.shape)
                print(y.shape)
                pix_weights_gm = pwg
                pix_weights_vent = pwv
                pix_weights = (pix_weights_gm + pix_weights_vent) * (pix_weights_vent > 0).astype(float)
                #pix_weights = pix_weights.transpose(0,3,1,2)
                optimizer.zero_grad()
                X = torch.from_numpy(X)
                y = torch.from_numpy(y)
                pix_weights = torch.from_numpy(pix_weights)
                X = X.to(device=device, dtype=torch.float32)
                y = y.to(device=device, dtype=torch.double)
                pix_weights = pix_weights.to(device=device, dtype=torch.float32)
                masks_pred = model(X)
                print('y and mask_pred dimensions!........')
                print(y.size())
                print(masks_pred.size())
                loss = criterion(masks_pred, y, weight=pix_weights)
                running_loss += loss.item()
                #writer.add_scalar('Loss/train', loss.item(), global_step)
                optimizer.zero_grad()
                loss.backward()
                # nn.utils.clip_grad_value_(net.parameters(), 0.1)
                optimizer.step()

                gstep += 1
                if j % 150 == 0:
                    val_score,_ = eval_truenet(valdata, model, batch_size, device)
                    scheduler.step(val_score)
                    #writer.add_scalar('learning_rate', optimizer.param_groups[0]['lr'], gstep)
        if save_checkpoint:
            try:
                os.mkdir(dir_checkpoint)
                #logging.info('Created checkpoint directory')
            except OSError:
                pass
            torch.save(model.state_dict(), dir_checkpoint + f'CP_epoch{epoch + 1}.pth')
    #writer.close()
           


def eval_truenet(testdata, model, batch_size, device, test=0, mode='axial'):
    model.eval()
    if test:
        testdata = append_data(testdata)
    nsteps = max(testdata[0].shape[0] // batch_size,1)
    prob_array = np.array([])
    gen_next_test_batch = batch_generator(testdata, batch_size, shuffle=False)
    dice_values = 0
    for i in range(nsteps):
        Xv, yv, pwgv, pwvv = next(gen_next_test_batch)
        Xv = Xv.transpose(0,3,1,2)
        print('Testing/validation dimensions.......................................')
        print(Xv.shape)
        print(yv.shape)
        pix_weights_gmv = pwgv
        pix_weights_ventv = pwvv
        pix_weightsv = (pix_weights_gmv + pix_weights_ventv) * (pix_weights_ventv > 0).astype(float)
        Xv = torch.from_numpy(Xv)
        Xv = Xv.to(device=device, dtype=torch.float32)
        val_pred = model(Xv)
        softmax = nn.Softmax()
        probs = softmax(val_pred)
        probs_vector = probs.contiguous().view(-1,2)
        mask_vector = (probs_vector[:,1] > 0.5).double()
        yv = torch.from_numpy(yv)
        yv = yv.to(device=device, dtype=torch.double)
        target_vector = yv.contiguous().view(-1)
        dice_val = dice_coeff(mask_vector, target_vector)
        probs1 = probs.cpu()
        probs_np = probs1.detach().numpy()
        
        prob_array = np.concatenate((prob_array,probs_np),axis=0) if prob_array.size else probs_np
                
        dice_values += dice_val
    prob_array = prob_array.transpose(0,2,3,1)
    dice_values = dice_values / (nsteps+1)
    return dice_values, prob_array


train_folder = '/path/to/training/files/'
val_folder = '/path/to/validation/files/'
test_folder = '/path/to/testing/files/'
results_directory = '/output/path/for/saving/results/'

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

model_axial = TrUENet(n_channels=2, n_classes=2, batch_size=8, init_channels=64, plane='axial')
model_sagittal = TrUENet(n_channels=2, n_classes=2, batch_size=8, init_channels=64, plane='sagittal')
model_coronal = TrUENet(n_channels=2, n_classes=2, batch_size=8, init_channels=64, plane='coronal')

model_axial.to(device=device)
model_sagittal.to(device=device)
model_coronal.to(device=device)

train_truenet(train_folder, val_folder, model_axial, 8, 2, device, mode='axial')
train_truenet(train_folder, val_folder, model_sagittal, 8, 2, device, mode='sagittal')
train_truenet(train_folder, val_folder, model_coronal, 8, 2, device, mode='coronal')

prob_image_3planes = []
test_names = glob.glob(test_folder+'*.npz')
brains, data, data_t1, labels, GM_distance, ventdistmap = data_preparation_pytorch.create_data_array_from_loaded_data_ox(test_names, plane='axial')
testdata = data_preparation_pytorch.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap, plane='axial', test=1)

dice_axial, prob_array_axial = eval_truenet(testdata, model_axial, 8, device, test=1, mode='axial')

print('Testdata Axial dimensions............................................')
print(testdata[0].shape)  
print('Probability dimensions............................................')
print(prob_array_axial.shape)

prob_vals1_axial = 0*testdata[0]
prob_vals1_axial[:prob_array_axial.shape[0],:prob_array_axial.shape[1],:prob_array_axial.shape[2],:] = prob_array_axial
prob_vals1_axial = data_postprocessing.resize_to_original_size_ox(prob_vals1_axial,test_names,plane='axial')
prob_image_3planes.append(prob_vals1_axial)

brains, data, data_t1, labels, GM_distance, ventdistmap = data_preparation_pytorch.create_data_array_from_loaded_data_ox(test_names, plane='sagittal')
testdata = data_preparation_pytorch.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap, plane='sagittal', test=1)

dice_sagittal, prob_array_sagittal = eval_truenet(testdata, model_sagittal, 8, device, test=1, mode='sagittal')

print('Testdata Sagittal dimensions............................................')
print(testdata[0].shape)  
print('Probability dimensions............................................')
print(prob_array_sagittal.shape)

prob_vals1_sagittal = 0*testdata[0]
prob_vals1_sagittal[:prob_array_sagittal.shape[0],:prob_array_sagittal.shape[1],:prob_array_sagittal.shape[2],:] = prob_array_sagittal
prob_vals1_sagittal = data_postprocessing.resize_to_original_size_ox(prob_vals1_sagittal,test_names,plane='sagittal')
prob_image_3planes.append(prob_vals1_sagittal)

brains, data, data_t1, labels, GM_distance, ventdistmap = data_preparation_pytorch.create_data_array_from_loaded_data_ox(test_names, plane='coronal')
testdata = data_preparation_pytorch.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap, plane='coronal', test=1)

dice_coronal, prob_array_coronal = eval_truenet(testdata, model_coronal, 8, device, test=1, mode='coronal')

print('Testdata Coronal dimensions............................................')
print(testdata[0].shape)  
print('Probability dimensions............................................')
print(prob_array_coronal.shape)

prob_vals1_coronal = 0*testdata[0]
prob_vals1_coronal[:prob_array_coronal.shape[0],:prob_array_coronal.shape[1],:prob_array_coronal.shape[2],:] = prob_array_coronal
prob_vals1_coronal = data_postprocessing.resize_to_original_size_ox(prob_vals1_coronal,test_names,plane='coronal')
prob_image_3planes.append(prob_vals1_coronal)
    
print('Aggregates probability dimensions............................................')
print(prob_image_3planes[0].shape)
print(prob_image_3planes[1].shape)
print(prob_image_3planes[2].shape)

prob_image_3planes = np.array(prob_image_3planes)
          
prob_mean = np.mean(prob_image_3planes,axis = 0)
prob_mean = np.tile(prob_mean,(1,1,1,1))
prob_mean = prob_mean.transpose(1,2,3,0)
prob_mean = np.concatenate((prob_mean,prob_mean),axis=-1)
volumes_m = data_postprocessing.construct_3dvolumes_from_2dslices_cropped_ox(prob_mean,test_names)
del prob_mean
np.save(results_directory + 'Probs_results_Truenet',volumes_m)


